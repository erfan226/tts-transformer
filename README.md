# Transformer Text to Speech

## Requirements
  * Install python 3
  * Install pytorch == 0.4.0
  * Install requirements:
    ```
   	pip install -r requirements.txt (Already there if you use the provided notebook)
   	```

## Data
* Common Voice dataset (Version 3, Persian) which consists of pairs of text script and mp3 files. The complete dataset can be downloaded from [here](https://commonvoice.mozilla.org/en/datasets).
* Put the dataset (for training only) at data/ directory.

## Pretrained Model
* You can download pretrained model from [here](https://drive.google.com/file/d/1VjgAMDPtthMdB3hO7GObERHSvgF5aokW/view?usp=sharing) (Transformer model) and [here](https://drive.google.com/file/d/1B0kEDLdQt2vK-rAJZb6kbkehDkUxFo6v/view?usp=sharing) (Postnet model)
* Put the pretrained model(s) at checkpoint/ directory and then you can synthesisze your text.

## File description
  * `hyperparams.py` includes all hyper-parameters that are needed.
  * `prepare_data.py` preprocess wav files to mel, linear spectrogram and save them for faster training time. Preprocessing codes for text is in text/ directory.
  * `preprocess.py` includes all preprocessing codes when you load data.
  * `module.py` contains all methods, including attention, prenet, postnet and so on.
  * `network.py` contains networks including encoder, decoder and post-processing network.
  * `train_transformer.py` is for training autoregressive attention network. (text --> mel)
  * `train_postnet.py` is for training post network. (mel --> linear)
  * `synthesis.py` is for generating TTS sample.
  * `mp3_to_wav.py` is for generating wav files from mp3 and moving them to the appropriate directory.

## Training Phase
  * STEP 1. Download and extract Common Voice data at data directory.
  * STEP 2. Adjust hyperparameters in `hyperparams.py`, especially 'data_path' which is a directory that you extract files, and the others if necessary.
  * STEP 3. Run `prepare_data.py`.
  * STEP 4. Run `train_transformer.py`.
  * STEP 5. Run `train_postnet.py`.

## Generate TTS wav File
  * STEP 1. Run `synthesis.py`. Change the arguments (steps) if you have trained the model from scratch or sth else. 


## Sample Generated Sounds
Some sample synthesized files are available to check out in the samples dir.
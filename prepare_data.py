import numpy as np
import pandas as pd
from torch.utils.data import Dataset, DataLoader
import os
from utils import get_spectrograms
import hyperparams as hp
import librosa

class PrepareDataset(Dataset):
    """Common Voices dataset - Persian."""

    def __init__(self, csv_file, root_dir):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the wavs.

        """
        self.landmarks_frame = pd.read_csv(csv_file, error_bad_lines=False, dtype=str)
        self.root_dir = root_dir

    def load_wav(self, filename):
        return librosa.load(filename, sr=hp.sample_rate)

    def __len__(self):
        return len(self.landmarks_frame)

    def __getitem__(self, idx):
        # print(self.landmarks_frame["path"].to_list()[idx])
        wav_name = os.path.join(self.root_dir, self.landmarks_frame["path"].to_list()[idx]) + '.wav'
        mel, mag = get_spectrograms(wav_name)
        
        np.save("data/prepared_train/"+self.landmarks_frame["path"].to_list()[idx].replace(".mp3", "") + '.pt', mel)
        np.save("data/prepared_train/"+self.landmarks_frame["path"].to_list()[idx].replace(".mp3", "") + '.mag', mag)

        sample = {'mel':mel, 'mag': mag}

        return sample
    
if __name__ == '__main__':
    dataset = PrepareDataset(os.path.join(hp.data_path,'metadata.csv'), os.path.join(hp.data_path,'train'))
    dataloader = DataLoader(dataset, batch_size=1, drop_last=False, num_workers=0)
    from tqdm import tqdm
    pbar = tqdm(dataloader)
    for d in pbar:
        pass

from pydub import AudioSegment
import pandas as pd
from tqdm import tqdm
# import os, shutil

df = pd.read_csv("data/metadata.csv", error_bad_lines=False, dtype=str)
path_col = df["path"]

for i in tqdm(range(len(path_col))):
#     shutil.move("data/wavs/"+str(path_col[i])+".wav", "data/train/"+str(path_col[i])+".wav")

    song = AudioSegment.from_mp3("data/clips/"+path_col[i])
    song.export("data/train/"+path_col[i]+".wav", format="wav")
